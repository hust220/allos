#include <iostream>
#include <array>
#include <algorithm>
#include <string>
#include <cstring>
#include <vector>
#include <fstream>
#include <sstream>

using namespace std;

using Neighbors = vector<vector<int>>;
using Propensities = vector<double>;

/**
 * Tokenize a string.
 */
vector<string> string_tokenize(const string &str, const string &delimiters = " \t") {
    vector<string> tokens;
    auto lastPos = str.find_first_not_of(delimiters, 0);
    auto pos = str.find_first_of(delimiters, lastPos);
    while (string::npos != pos || string::npos != lastPos) {
        tokens.push_back(str.substr(lastPos, pos - lastPos));
        lastPos = str.find_first_not_of(delimiters, pos);
        pos = str.find_first_of(delimiters, lastPos);
    }
    return move(tokens);
}

/**
 * Parse a string to type T.
 */
template<typename T, typename U>
T string_parse(U && u) {
    stringstream stream;
    T t;

    stream << u;
    stream >> t;
    return t;
}

class Mat {
public:
    double *data;
    array<int, 2> shape;

    Mat() {
        data = NULL;
        shape[0] = 0;
        shape[1] = 0;
    }

    Mat(int a, int b) {
        shape[0] = a;
        shape[1] = b;
        data = new double[a * b];
    }

    Mat(const Mat &mat) {
        shape[0] = mat.shape[0];
        shape[1] = mat.shape[1];
        int size = shape[0] * shape[1];
        data = new double[size];
        for (int i = 0; i < size; i++) {
            data[i] = mat.data[i];
        }
    }

    Mat(Mat &&mat) {
        data = mat.data;
        mat.data = NULL;

        swap(shape, mat.shape);
    }

    Mat &operator =(const Mat &mat) {
        if (data != NULL) {
            delete [] data;
            data = NULL;
        }

        shape[0] = mat.shape[0];
        shape[1] = mat.shape[1];
        int size = shape[0] * shape[1];
        data = new double[size];
        for (int i = 0; i < size; i++) {
            data[i] = mat.data[i];
        }
    }

    Mat &operator =(Mat &&mat) {
        data = mat.data;
        mat.data = NULL;

        swap(shape, mat.shape);
    }

    ~Mat() {
        if (data != NULL) {
            delete [] data;
            data = NULL;
        }
    }

    int rows() const {
        return shape[0];
    }

    int cols() const {
        return shape[1];
    }

    double &operator ()(int i, int j) {
        return data[i * shape[1] + j];
    }

    const double &operator ()(int i, int j) const {
        return data[i * shape[1] + j];
    }
};

Mat read_mat(const string &mat_file) {
    ifstream ifile(mat_file.c_str());
    int a, b;
    ifile >> a >> b;
    Mat mat(a, b);
    for (int i = 0; i < a; i++) {
        for (int j = 0; j < b; j++) {
            ifile >> mat(i, j);
        }
    }
    ifile.close();

    return move(mat);
}

Neighbors get_neighbors(const Mat &mat, double cutoff) {
    int n = mat.rows();
    Neighbors neighbors(n);
    for (int i = 0; i < n; i++) {
        for (int j = i + 1; j < n; j++) {
            if (mat(i, j) > cutoff) {
                neighbors[i].push_back(j);
                neighbors[j].push_back(i);
            }
        }
    }
    return move(neighbors);
}

class PathIterator {
public:
    const Mat &mat;
    const Neighbors &neighbors;
    int beg;
    int end;
    double cutoff;
    int num_bonds;

    int n;
    vector<int> path;
    vector<int> index;

    PathIterator(const Mat &mat_, const Neighbors &neighbors_, int beg_, int end_, double cutoff_, int num_bonds_) :
        mat(mat_), neighbors(neighbors_), beg(beg_), end(end_), cutoff(cutoff_), num_bonds(num_bonds_)
    {
        n = mat.rows();
        index.resize(num_bonds - 1, 0);
        index.back() = -1;

        path.resize(num_bonds - 1);
        path[0] = neighbors[beg][0];
        for (int i = 1; i < num_bonds - 1; i++) {
            path[i] = neighbors[path[i-1]][0];
        }
    }

    bool next() {
        int prev;

        for (int i = num_bonds - 1; i >= 0; i--) {
            index[i]++;

            prev = (i == 0 ? beg : path[i - 1]);

            if (index[i] >= neighbors[prev].size()) {
                if (i == 0) {
                    return false;
                }
                else {
                    index[i] = 0;
                    continue;
                }
            }
            else {
                for (int j = i; j < num_bonds; j++) {
                    if (j == 0) {
                        path[j] = neighbors[beg][index[j]];
                    }
                    else {
                        path[j] = neighbors[path[j-1]][index[j]];
                    }
                }
                break;
            }
        }

        if (mat(path.back(), end) < cutoff) {
            return next();
        } else {
            return true;
        }
    }

};

Propensities get_propensities(const Mat &mat, int asite, const Neighbors &neighbors, int num_bonds, double cutoff) {
    int n = mat.rows();
    Propensities props(n, 0);
    if (num_bonds == 1) {
        for (int i = 0; i < n; i++) {
            props[i] = mat(asite, i);
        }
    }
    else {
        for (int i = 0; i < n; i++) {
            if (i != asite) {
                double sum = 0;
                PathIterator it(mat, neighbors, asite, i, cutoff, num_bonds);
                while (it.next()) {
                    if (none_of(it.path.begin(), it.path.end(), [asite, i](int j){
                        return j == asite || j == i;
                    })) {
                        double m = mat(asite, it.path[0]);
                        for (int j = 0; j < num_bonds - 2; j++) {
                            m *= mat(it.path[j], it.path[j + 1]);
                        }
                        m *= mat(it.path.back(), i);
                        sum = 1 - (1 - sum) * (1 - m);
                    }
                }
                props[i] = sum;
            }
        }
    }
    return move(props);
}

void print_propensities(const Propensities &props, const string &out_file) {
    ofstream ofile(out_file.c_str());
    for (auto && prop : props) {
        ofile << prop << endl;
    }
    ofile.close();
}

int main(int argc, char **argv) {
    string mat_file = argv[1];
    string out_file = argv[2];
    int asite = string_parse<int>(argv[3]) - 1; // allosteric site

    int num_bonds = 2;
    if (argc >= 5) {
        num_bonds = string_parse<int>(argv[4]);
    }

    double cutoff = 0.3;
    if (argc >= 6) {
        cutoff = string_parse<double>(argv[5]);
    }

    auto mat = read_mat(mat_file);

    auto neighbors = get_neighbors(mat, cutoff);

    auto props2 = get_propensities(mat, asite, neighbors, num_bonds, cutoff);
    print_propensities(props2, out_file);

    return 0;
}


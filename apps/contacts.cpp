#include <iostream>
#include <jnc/bio>
#include <jnc/math>
#include <numeric>

int main(int argc, char **argv) {
    jnc::Opt opt(argc, argv);

    jnc::pdb::Pdb pdb(opt.argv[1]);

    std::vector<std::string> backbone_atoms {"N", "CA", "C", "O"};
    auto & bb = backbone_atoms;

    // Output indices
    if (opt.has("oi")) {
        auto oi_name = opt.get("oi");
        std::ofstream oifile(oi_name.c_str());
        int n_res = 0;
        int n_chain = 0;
        for (auto && chain : pdb[0]) {
            int n_res_in_chain = 0;
            for (auto && res : chain) {
                oifile << n_res + 1 << ' ' << n_chain + 1 << '/' << chain.name << ' ' << res.num << '/' << res.name << std::endl;
                n_res++;
                n_res_in_chain++;
            }
            n_chain++;
        }
        oifile.close();
    }

    // Calculate contacts
    auto rs = pdb[0].presidues();

    int n = rs.size();

    double cutoff = 3.5;
    opt.set(cutoff, "c", "cutoff");
//    int threshold = 1;

    double alpha = 1.0;
    opt.set(alpha, "a", "alpha");
//    opt.set(threshold, "t", "threshold");

    jnc::Mat contacts = jnc::Mat::Zero(n, n);

    for (int i = 0; i < n; i++) {
        auto &r1 = *(rs[i]);
        for (int j = i + 1; j < n; j++) {
            auto &r2 = *(rs[j]);

            int n1 = r1.size();
            int n2 = r2.size();

            int sum = 0;
            for (int i1 = 0; i1 < n1; i1++) {
                for (int i2 = 0; i2 < n2; i2++) {
                    double d = jnc::distance(r1[i1], r2[i2]);
                    if (d <= cutoff) {
                        // Exclude backbone-backbone contacts
                        if (std::all_of(bb.begin(), bb.end(), [&r1, i1](const std::string &name) {
                            return name != r1[i1].name;
                        }) || std::all_of(bb.begin(), bb.end(), [&r2, i2](const std::string &name) {
                            return name != r2[i2].name;
                        })) {
                            sum++;
                        }
                    }
                }
            }

            contacts(i, j) = 1 - std::exp(-alpha * sum / double(n2));
            contacts(j, i) = 1 - std::exp(-alpha * sum / double(n1));
        }
    }

    // Output contacts
    auto oc_name = opt.get("oc");
    std::ofstream ocfile(oc_name.c_str());
    ocfile << n << ' ' << n << std::endl;
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            ocfile << contacts(i, j) << ' ';
        }
        ocfile << std::endl;
    }
    ocfile.close();

    return 0;
}


#include <iostream>
#include <jnc/bio>
#include <jnc/math>
#include <numeric>
#include "pocket_finder.hpp"

int main(int argc, char **argv) {
    jnc::Opt opt(argc, argv);

    PocketFinder pocket_finder(opt);
    pocket_finder.run();

    return 0;
}


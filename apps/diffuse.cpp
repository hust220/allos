#include <iostream>
#include <array>
#include <algorithm>
#include <random>
#include <string>
#include <cstring>
#include <vector>
#include <fstream>
#include <sstream>
#include <utility>
#include <functional>
#include <map>

#include <jnc/core>

using namespace std;

mt19937 rand_engine { 11 };
uniform_real_distribution<double> unif_distr { 0, 1 };

/**
 * Generate a random float number between 0 and 1.
 */
void my_seed(int seed) {
    rand_engine.seed(seed);
}

double my_rand() {
    return unif_distr(rand_engine);
}

/**
 * Tokenize a string.
 */
vector<string> string_tokenize(const string &str, const string &delimiters = " \t") {
    vector<string> tokens;
    auto lastPos = str.find_first_not_of(delimiters, 0);
    auto pos = str.find_first_of(delimiters, lastPos);
    while (string::npos != pos || string::npos != lastPos) {
        tokens.push_back(str.substr(lastPos, pos - lastPos));
        lastPos = str.find_first_not_of(delimiters, pos);
        pos = str.find_first_of(delimiters, lastPos);
    }
    return move(tokens);
}

/**
 * Parse a string to type T.
 */
template<typename T, typename U>
T string_parse(U && u) {
    stringstream stream;
    T t;

    stream << u;
    stream >> t;
    return t;
}

template<typename T>
class Mat {
public:
    using data_type = T;

    data_type *data;
    array<int, 2> shape;

    Mat() {
        data = NULL;
        shape[0] = 0;
        shape[1] = 0;
    }

    Mat(int a, int b) {
        shape[0] = a;
        shape[1] = b;
        data = new data_type[a * b];
    }

    Mat(int a, int b, const data_type &v) {
        shape[0] = a;
        shape[1] = b;
        data = new data_type[a * b];
        for (int i = 0; i < a * b; i++) {
            data[i] = v;
        }
    }

    Mat(const Mat &mat) {
        shape[0] = mat.shape[0];
        shape[1] = mat.shape[1];
        int size = shape[0] * shape[1];
        data = new data_type[size];
        for (int i = 0; i < size; i++) {
            data[i] = mat.data[i];
        }
    }

    Mat(Mat &&mat) {
        data = mat.data;
        mat.data = NULL;

        swap(shape, mat.shape);
    }

    Mat &operator =(const Mat &mat) {
        if (data != NULL) {
            delete [] data;
            data = NULL;
        }

        shape[0] = mat.shape[0];
        shape[1] = mat.shape[1];
        int size = shape[0] * shape[1];
        data = new data_type[size];
        for (int i = 0; i < size; i++) {
            data[i] = mat.data[i];
        }
    }

    Mat &operator =(Mat &&mat) {
        data = mat.data;
        mat.data = NULL;

        swap(shape, mat.shape);
    }

    ~Mat() {
        if (data != NULL) {
            delete [] data;
            data = NULL;
        }
    }

    int rows() const {
        return shape[0];
    }

    int cols() const {
        return shape[1];
    }

    data_type &operator ()(int i, int j) {
        return data[i * shape[1] + j];
    }

    const data_type &operator ()(int i, int j) const {
        return data[i * shape[1] + j];
    }
};

using Matd = Mat<double>;
using Matb = Mat<bool>;

using Neighbors = vector<vector<int>>;
using NodePropensities = vector<double>;
using BondPropensities = Matd;
using Path = deque<int>;
using PathPropensities = map<Path, double>;

Matd read_mat(const string &mat_file) {
    ifstream ifile(mat_file.c_str());
    int a, b;
    ifile >> a >> b;
    Matd mat(a, b);
//    double max = 0;
    for (int i = 0; i < a; i++) {
        for (int j = 0; j < b; j++) {
            double d;
            ifile >> d;
            mat(i, j) = d;
//            if (max < d) {
//                max = d;
//            }
        }
    }
    ifile.close();

//    for (int i = 0; i < a; i++) {
//        for (int j = 0; j < b; j++) {
//            mat(i, j) /= max;
//        }
//    }

    return move(mat);
}

Neighbors get_neighbors(const Matd &mat, double cutoff) {
    int n = mat.rows();
    Neighbors neighbors(n);
    for (int i = 0; i < n; i++) {
        for (int j = i + 1; j < n; j++) {
            if (mat(i, j) > cutoff) {
                neighbors[i].push_back(j);
                neighbors[j].push_back(i);
            }
        }
    }
    return move(neighbors);
}

class Diffuse {
public:
    const Matd &mat;
    const Neighbors &neighbors;
    Matb check_bond;
    Matb through_bond;
    vector<bool> through_node;

    int n;

    Diffuse(const Matd &mat_, const Neighbors &neighbors_) : mat(mat_), neighbors(neighbors_) {
        n = mat_.rows();
        check_bond = Matb(n, n, false);
        through_bond = Matb(n, n, false);
        through_node.resize(n, false);
    }

    void diffuse(int beg) {
        if (!through_node[beg]) {
            through_node[beg] = true;
            for (auto && i : neighbors[beg]) {
                if (!check_bond(beg, i)) {
                    check_bond(beg, i) = check_bond(i, beg) = true;
                    if (my_rand() < mat(beg, i)) {
                        through_bond(beg, i) = through_bond(i, beg) = true;
                        diffuse(i);
                    }
                }
            }
        }
    }

};

class PathFinder {
public:
    using Pair = pair<Path, vector<double>>;
    const Neighbors &neighbors;

    Matd en;
    vector<bool> visited;
    vector<double> distance;
    vector<int> previous;

    deque<Pair> container_a;
    deque<Pair> container_b;

    int n;

    Path path;
    vector<Path> paths;
    vector<vector<double>> distances;
    vector<double> scores;

    PathFinder(const Matd &mat_, const Neighbors &neighbors_) : en(mat_), neighbors(neighbors_) {
        n = mat_.rows();

        for (int i = 0; i < n * n; i++) {
            if (en.data[i] < 0.0001) {
                en.data[i] = -std::log(0.0001);
            } else {
                en.data[i] = -std::log(en.data[i]);
            }
        }

        distance.resize(n, 999999);
        previous.resize(n, -1);
        visited.resize(n, false);
    }

    void run(int start, int end, int n_paths) {
        distance[start] = 0;
        find_path(start, end);

        auto && path = get_path(start, end);

        vector<double> path_distance;
        for (int i = 0; i < path.size(); i++) path_distance.push_back(distance[path[i]]);

        container_a.push_back(make_pair(path, path_distance));

        for (int i = 1; i < n_paths; i++) {
//            cout << start << '-' << end << '-' << i << endl;
            auto path_k = container_a[i - 1].first;
            auto distance_k = container_a[i - 1].second;

            for (int j = 0; j < path_k.size() - 1; j++) {
                // reset previous
                for (auto && k : previous) k = -1;

                // reset visited
                for (auto && k : visited) k = false;
                for (int k = 0; k < j; k++) visited[path_k[k]] = true;

                // reset distance
                for (auto && k : distance) k = 999999;

                distance[path_k[j]] = 0;
                double old_edge = en(path_k[j], path_k[j + 1]);
                en(path_k[j], path_k[j + 1]) = 999999;
                find_path(path_k[j], end);
//                for (int k = 0; k < n; k++) {
//                    if (previous[k] != -1) {
//                        cout << k << ' ';
//                    }
//                }
//                cout << ": " << path_k[j] << ' ' << end << endl;
                if (previous[end] == -1) continue; // in case no path is found
                auto && path2 = get_path(path_k[j], end);
                en(path_k[j], path_k[j + 1]) = old_edge;

                Path path_new;
                vector<double> distance_new;

                for (int k = 0; k <= j; k++) {
                    path_new.push_back(path_k[k]);
                    distance_new.push_back(distance_k[k]);
                }
                for (int k = 1; k < path2.size(); k++) {
                    path_new.push_back(path2[k]);
                    distance_new.push_back(distance[path2[k]]);
                }

                container_b.push_back(make_pair(path_new, distance_new));
            }

            sort(container_b.begin(), container_b.end(), [](const Pair &a, const Pair &b){
                return a.second.back() < b.second.back();
            });

            container_a.push_back(container_b[0]);
            container_b.pop_front();
        }
    }

    Path get_path(int start, int end) {
        Path path;
        path.push_back(end);
        int i = end;
        do {
//            std::cout << i << ' ' << previous[i] << std::endl;
            i = previous[i];
            path.push_front(i);
        } while (i != start);
        return move(path);
    }

    void find_path(int beg, int end) {
        visited[beg] = true;

//        std::cout << std::count(visited.begin(), visited.end(), true) << std::endl;

//        if (std::all_of(end_sites.begin(), end_sites.end(), [this](int i){ return visited[i]; })) {}
        if (beg == end) {
            return;
        } else {
            double d = distance[beg];
            for (auto && i : neighbors[beg]) {
                if (!visited[i]) {
                    double di = distance[i];
                    double dd = d + en(beg, i);
                    if (di > dd) {
                        distance[i] = dd;
                        previous[i] = beg;
                    }
                }
            }

            for (auto && i : neighbors[beg]) {
                if (!visited[i]) {
                    find_path(i, end);
                }
            }
        }
    }
};

void set_propensities(NodePropensities &node_props, BondPropensities &bond_props, const Matd &mat, const vector<int> &sites, double cutoff, int steps) {
    auto neighbors = get_neighbors(mat, cutoff);

    int n = mat.rows();
    for (int step = 0; step < steps; step++) {
        Diffuse diffuse(mat, neighbors);
        for (auto && site: sites) {
            diffuse.diffuse(site);
        }

        for (int i = 0; i < n; i++) {
            if (diffuse.through_node[i]) {
                node_props[i]++;
            }
        }

        for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
                if (diffuse.through_bond(i, j)) {
                    bond_props(i, j)++;
                    bond_props(j, i)++;
                }
            }
        }
    }

    double max = 0;
    for (int i = 0; i < n; i++) {
        if (max < node_props[i]) {
            max = node_props[i];
        }
    }
    for (int i = 0; i < n; i++) {
        node_props[i] /= max;
    }

    max = 0;
    for (int i = 0; i < n; i++) {
        for (int j = i + 1; j < n; j++) {
            double d = bond_props(i, j);
            if (max < d) {
                max = d;
            }
        }
    }
    for (int i = 0; i < n; i++) {
        for (int j = i + 1; j < n; j++) {
            bond_props(i, j) /= max;
            bond_props(j, i) /= max;
        }
    }
}

Matd get_correlations(const Matd &mat, double cutoff, int steps) {
    auto neighbors = get_neighbors(mat, cutoff);

    int n = mat.rows();

    Matd corr(n, n, 0);

    for (int site = 0; site < n; site++) {
        NodePropensities node_props(n, 0);

        for (int step = 0; step < steps; step++) {
            Diffuse diffuse(mat, neighbors);
            diffuse.diffuse(site);

            for (int j = 0; j < n; j++) {
                if (diffuse.through_node[j]) {
                    node_props[j]++;
                }
            }
        }

        double max = 0;
        for (int i = 0; i < n; i++) {
            if (max < node_props[i]) {
                max = node_props[i];
            }
        }

        for (int i = 0; i < n; i++) {
            node_props[i] /= max;
            corr(site, i) = node_props[i];
        }
    }

    return move(corr);
}

PathPropensities get_path_propensities(const Matd &mat, const vector<int> &start_sites, const vector<int> &end_sites, double cutoff, int n_paths) {
    auto neighbors = get_neighbors(mat, cutoff);

    int n = mat.rows();

    PathPropensities path_info;
    for (auto && start : start_sites) {
        for (auto && end : end_sites) {
            PathFinder finder(mat, neighbors);
            finder.run(start, end, n_paths);
            for (auto && p : finder.container_a) {
                path_info[p.first] = exp(-p.second.back());
            }
        }
    }

//    double max = 0;
//    for (auto && p : diffuse.path_info) {
//        if (max < p.second) {
//            max = p.second;
//        }
//    }

//    for (auto && p : path_info) {
//        p.second /= steps;
//    }

    return move(path_info);
}

void print_correlations(const Matd &corr, const string &out_file) {
    ofstream ofile(out_file.c_str());
    int n = corr.rows();
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            ofile << corr(i, j) << ' ';
        }
        ofile << std::endl;
    }
    ofile.close();
}

void print_node_propensities(const NodePropensities &props, const string &out_file) {
    ofstream ofile(out_file.c_str());
    for (auto && prop : props) {
        ofile << prop << endl;
    }
    ofile.close();
}

void print_bond_propensities(const BondPropensities &props, const string &out_file) {
    int n = props.rows();
    ofstream ofile(out_file.c_str());
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            ofile << props(i, j) << ' ';
        }
        ofile << endl;
    }
    ofile.close();
}

void print_path_propensities(const PathPropensities &props, const string &out_file) {
    // Transform to a vector
    vector<pair<Path, double>> v;
    for (auto && p : props) {
        v.push_back(p);
    }

    // Sort
    sort(v.begin(), v.end(), [](auto && p1, auto && p2){
        return p1.second > p2.second;
    });

    // Print
    ofstream ofile(out_file.c_str());
    for (auto && p : v) {
        for (auto && i : p.first) {
            i++;
        }
        ofile << p.second << ' ' << jnc::string_join('-', p.first) << endl;
    }
    ofile.close();
}

vector<int> get_sites(const string &str) {
    vector<int> sites;
    auto && v = string_tokenize(str, "+");
    for (auto && s : v) {
        auto && w = string_tokenize(s, "-");
        if (w.size() == 1) {
            sites.push_back(string_parse<int>(s) - 1);
        } else if (w.size() == 2) {
            int l = string_parse<int>(w[0]) - 1;
            int u = string_parse<int>(w[1]) - 1;
            for (int i = l; i <= u; i++) {
                sites.push_back(i);
            }
        }
    }
    return move(sites);
}

/**
 * Example: ./diffuse mat.txt nodes.txt bonds.txt 1 10000 0.3
 */
int main(int argc, char **argv) {
    jnc::Opt opt(argc, argv);

    int seed = 11;
    if (opt.has("seed", "S")) {
        opt.set(seed, "seed", "S");
    }
    my_seed(seed);
    
    if (opt.g[0] == "path") {
        string mat_file = opt.g[1];

        auto && start_sites = get_sites(opt.g[2]);
        auto && end_sites = get_sites(opt.g[3]);

        string path_out_file = opt.g[4];

        int n = 100;
        opt.set(n, "n");

        double cutoff = 0.05;
        opt.set(cutoff, "c", "cutoff");

        auto mat = read_mat(mat_file);

        auto && path_propensities = get_path_propensities(mat, start_sites, end_sites, cutoff, n);

        print_path_propensities(path_propensities, path_out_file);

    } else if (opt.g[0] == "all") {
        string mat_file = opt.g[1];

        string out_file = opt.g[2];

        int steps = 1000;
        opt.set(steps, "n", "steps");

        double cutoff = 0.05;
        opt.set(cutoff, "c", "cutoff");

        auto mat = read_mat(mat_file);

        auto && correlations = get_correlations(mat, cutoff, steps);

        print_correlations(correlations, out_file);

    } else {
        string mat_file = argv[1];
        auto && sites = get_sites(argv[2]);

        string node_out_file = argv[3];
        string bond_out_file = argv[4];

        int steps = 10000;
        opt.set(steps, "n", "steps");

        double cutoff = 0.05;
        opt.set(cutoff, "c", "cutoff");

        auto mat = read_mat(mat_file);

        int n = mat.rows();

        NodePropensities node_props(n, 0);
        BondPropensities bond_props(n, n, 0);

        set_propensities(node_props, bond_props, mat, sites, cutoff, steps);

        print_node_propensities(node_props, node_out_file);
        print_bond_propensities(bond_props, bond_out_file);

    }
    return 0;
}


#include <iostream>
#include <jnc/bio>
#include <jnc/math>
#include <numeric>

int main(int argc, char **argv) {
    jnc::Opt opt(argc, argv);

    std::string in_pdb = opt.argv[1];
    std::string bfactor_file = opt.argv[2];
    std::string out_pdb = opt.argv[3];

    // Read PDB
    jnc::pdb::Pdb pdb(in_pdb);

    auto rs = pdb[0].presidues();
    int n = rs.size();

    // Read and set bfactors
    std::ifstream ifile(bfactor_file.c_str());
    for (int i = 0; i < n; i++) {
        double d;
        ifile >> d;
        auto &r = *(rs[i]);
        for (auto && atom : r) {
            atom.bfactor = d;
        }
    }
    ifile.close();

    // Write PDB
    std::ofstream ofile(out_pdb.c_str());
    ofile << pdb;
    ofile.close();

    return 0;
}


#include <iostream>
#include <jnc/bio>
#include <jnc/math>
#include <numeric>

static std::set<int> get_sites(const std::string &str) {
    std::set<int> sites;
    auto && v = jnc::string_tokenize(str, "+ ");
    for (auto && s : v) {
        auto && w = jnc::string_tokenize(s, "- ");
        if (w.size() == 1) {
            sites.insert(JN_INT(s) - 1);
        } else if (w.size() == 2) {
            int l = JN_INT(w[0]) - 1;
            int u = JN_INT(w[1]) - 1;
            for (int i = l; i <= u; i++) {
                sites.insert(i);
            }
        }
    }
    return std::move(sites);
}

static auto read_list(const std::string &list_name) {
    std::set<int> ls;
    std::string n;
    std::ifstream ifile(list_name.c_str());
    while (ifile >> n) {
        ls.insert(JN_INT(n)-1);
    }
    ifile.close();
    return ls;
}

int main(int argc, char **argv) {
    jnc::Opt opt(argc, argv);

    auto pdb_name = opt.g[0];
    auto list_name = opt.g[1];
    auto out_name = opt.g[2];

    jnc::pdb::Pdb pdb(opt.argv[1]);

    auto ls = get_sites(list_name);

    jnc::pdb::Pdb newPdb;
    jnc::pdb::Model model;
    int i = 0;
    for (auto && chain : pdb[0]) {
        jnc::pdb::Chain newChain;
        for (auto && residue : chain) {
            if (ls.find(i) != ls.end()) {
                newChain.push_back(residue);
            }
            i++;
        }
        if (newChain.size() > 0) {
            model.push_back(newChain);
        }
    }
    newPdb.push_back(model);

    std::ofstream ofile(out_name.c_str());
    ofile << newPdb;
    ofile.close();

    return 0;
}


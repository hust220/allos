#include <iostream>
#include <jnc/bio>
#include <jnc/math>
#include <numeric>

template<typename T>
std::array<double, 3> residue_center(const T &residue) {
    std::array<double, 3> c {0, 0, 0};
    int n = 0;
    for (auto && atom : residue) {
        for (int i = 0; i < 3; i++) {
            c[i] += atom[i];
        }
        n++;
    }
    for (int i = 0; i < 3; i++) {
        c[i] /= n;
    }
    return std::move(c);
}

// template<typename T, typename Path_, typename C_>
// std::string plot_path(const T &rs, const Path_ &path, const C_ &c1, const C_ &c2) {
template<typename T, typename Path_>
std::string plot_path(const T &rs, const Path_ &path) {
    int n = path.first.size();

    std::vector<std::array<double, 3>> c;
//    c.push_back(c1);
    for (auto && i : path.first) {
//        std::cerr << i << ' ' << rs.size() << ' ' << rs[i]->size() << std::endl;
        c.push_back(residue_center(*(rs[i])));
    }
//    c.push_back(c2);

    auto &c1 = c.front();
    auto &c2 = c.back();

    double width = 2 * path.second;
    double half_width = width / 2.0;

    std::string start_color = "0.57, 0.05, 0.25";
    std::string path_color = "0.094, 0.621, 0.020";
    std::string end_color = "0.02, 0.22, 0.59";

    std::stringstream ss;
    ss << "cyl = [\n"
       << "COLOR, " << path_color << ",\n"
       << jnc::string_format("SPHERE, %.2f, %.2f, %.2f, %f, \n", c[0][0], c[0][1], c[0][2], half_width)
       << jnc::string_format("25.0, 0.8, CYLINDER, %.2f, %.2f, %.2f, %.2f, %.2f, %.2f, %f, %s, %s,\n", c[0][0], c[0][1], c[0][2], c[1][0], c[1][1], c[1][2], half_width, path_color, path_color);
//       << "25.0, 0.8, CYLINDER, " << c[0][0] << ", " << c[0][1] << ", " << c[0][2] << ", " << c[1][0] << ", " << c[1][1] << ", " << c[1][2] << ", " << half_width << ", " << path_color << ", " << path_color << ",\n";

    for (int i = 1; i < n - 1; i++) {
        ss << "COLOR, " << path_color << ",\n"
           << jnc::string_format("SPHERE, %.2f, %.2f, %.2f, %f, \n", c[i][0], c[i][1], c[i][2], half_width)
//           << "SPHERE, " << c[i][0] << ", " << c[i][1] << ", " << c[i][2] << ", " << half_width << ",\n"
           << jnc::string_format("25.0, 0.8, CYLINDER, %.2f, %.2f, %.2f, %.2f, %.2f, %.2f, %f, %s, %s,\n", c[i][0], c[i][1], c[i][2], c[i+1][0], c[i+1][1], c[i+1][2], half_width, path_color, path_color);
//           << "25.0, 0.8, CYLINDER, " << c[i][0] << ", " << c[i][1] << ", " << c[i][2] << ", " << c[i+1][0] << ", " << c[i+1][1] << ", " << c[i+1][2] << ", " << half_width << ", " << path_color << ", " << path_color << ",\n";
    }

    ss << "COLOR, " << path_color << ",\n"
//       << "SPHERE, " << c2[0] << ", " << c2[1] << ", " << c2[2] << ", " << half_width << "\n"
       << jnc::string_format("SPHERE, %.2f, %.2f, %.2f, %f\n", c2[0], c2[1], c2[2], half_width)
       << "]\n";

    return ss.str();

}

static std::vector<int> get_sites(const std::string &str) {
    std::vector<int> sites;
    auto && v = jnc::string_tokenize(str, "+");
    for (auto && s : v) {
        auto && w = jnc::string_tokenize(s, "-");
        if (w.size() == 1) {
            sites.push_back(JN_INT(s) - 1);
        } else if (w.size() == 2) {
            int l = JN_INT(w[0]) - 1;
            int u = JN_INT(w[1]) - 1;
            for (int i = l; i <= u; i++) {
                sites.push_back(i);
            }
        }
    }
    return std::move(sites);
}

int main(int argc, char **argv) {
    jnc::Opt opt(argc, argv);

    std::string pdb_file = opt.argv[1];
    std::string cluster_file = opt.argv[2];
    std::string path_file = opt.argv[3];
    std::string out_file = opt.argv[4];

    int max = 100;
    opt.set(max, "n");

    auto pdb_name = jnc::path_splitext(jnc::path_basename(pdb_file))[0];
    auto out_name = jnc::path_splitext(out_file)[0];

    jnc::pdb::Pdb pdb(pdb_file);
    auto rs = pdb[0].presidues();

    // Clusters
    std::ifstream ifile(cluster_file.c_str());
    std::ofstream ofile(out_file.c_str());
    ofile << "from pymol.cgo import *" << std::endl;
    ofile << "cmd.bg_color('white')" << std::endl;
    ofile << jnc::string_format("cmd.load('%s')", pdb_file) << std::endl;
//    ofile << jnc::string_format("cmd.show_as('cartoon', '%s')", pdb_name) << std::endl;
    ofile << "cmd.color('white', 'all')" << std::endl;

    std::string line;
    int n_cluster = 0;
    while (ifile) {
        std::getline(ifile, line);
        auto &&v = jnc::string_tokenize(line, ":");
        if (v.size() == 2) {
            auto &&w = jnc::string_tokenize(v[0], " ");
            if (w.size() == 3) {
                auto &&u = jnc::string_tokenize(v[1], " ");
                std::vector<std::string> selection;
                for (auto && i : u) {
                    auto &&j = jnc::string_tokenize(i, "-");
                    if (j.size() == 2) {
                        selection.push_back(jnc::string_format("(chain %s and resi %s)", j[0], j[1]));
                    }
                }
                auto &&cluster_selection = jnc::string_join(" or ", selection);

                auto &&cluster_color = w[2];
                auto &&cluster_name = jnc::string_format("cluster-%d", n_cluster + 1);
                ofile << jnc::string_format("cmd.select('%s', '%s')", cluster_name, cluster_selection) << std::endl;
                ofile << jnc::string_format("h = '%s'", cluster_color) << std::endl;
                ofile << "h = h.lstrip('#')" << std::endl;
                auto &&color_name = jnc::string_format("cluster_%d_color", n_cluster + 1);
                ofile << jnc::string_format("%s = [int(h[i:i+2], 16) for i in (0, 2, 4)]", color_name) << std::endl;
                ofile << jnc::string_format("cmd.set_color('%s', %s)", color_name, color_name) << std::endl;
                ofile << jnc::string_format("cmd.color('%s', '%s')", color_name, cluster_name) << std::endl;
                n_cluster++;
            }
        }
    }

    ifile.close();
   
    ifile.open(path_file.c_str());
    std::map<std::vector<int>, double> paths;
    while (ifile) {
        std::getline(ifile, line);
        auto v = jnc::string_tokenize(line, " ");
        if (v.size() == 2) {
            auto weight = JN_DBL(v[0]);

            std::vector<int> path;
            auto w = jnc::string_tokenize(v[1], "-");
            for (auto && i : w) {
                double j = JN_INT(i) - 1;
//                if (std::find(sites1.begin(), sites1.end(), j) == sites1.end() && std::find(sites2.begin(), sites2.end(), j) == sites2.end()) {
                    path.push_back(j);
//                }
            }

            if (!paths.count(path)) {
                paths[path] = 0.0;
            }

            paths[path] += weight;
        }
    }
    ifile.close();

    std::vector<std::pair<std::vector<int>, double>> vpath;
    double weightMax = 0;
    for (auto && p : paths) {
        vpath.push_back(p);
        if (weightMax < p.second) {
            weightMax = p.second;
        }
    }

    for (auto && p : vpath) {
        p.second /= weightMax;
    }

    std::sort(vpath.begin(), vpath.end(), [](auto && p1, auto && p2) {
        return p1.second > p2.second;
    });

    for (int i = 0; i < vpath.size() && i < max; i++) {
//        ofile << plot_path(rs, vpath[i], c1, c2);
        ofile << plot_path(rs, vpath[i]);
        ofile << "cmd.load_cgo(cyl, 'path" << i+1 << "')\n" << std::endl;
    }

    ofile << "cmd.zoom('all',animate=-1)\n"
          << "cmd.save('" << out_name << ".pse')\n"
          << std::endl;

    ofile.close();

    return 0;
}


module load anaconda
module load allos

name=${1}
sites1=${2}
sites2=${3}

contacts ${name}.pdb -oi ${name}.ind -oc ${name}.mat -c 3.5
diffuse path ${name}.mat $sites1 $sites2 ${name}.paths -n 10000 -c 0.05
# gdb -args diffuse path ${name}.mat $sites1 $sites2 ${name}.paths -n 10000 -c 0.05

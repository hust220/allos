module load anaconda

name=ptp

../../bin/contacts ${name}.pdb -oi ${name}.ind -oc ${name}.mat -c 3.5
../../bin/diffuse ${name}.mat ${name}.nodes ${name}.paths 299 10000 0.05
python ../plot-nodes.py ${name}.nodes

module load anaconda
module load allos

name=${1}
sites1=$(cat sites1)
sites2=$(cat sites2)

echo "Name: $name"
echo "Sites: $sites1 $sites2"

contacts ${name}.pdb -oi ${name}.ind -oc ${name}.mat -c 3.4 -a 3
perl -lane '$n++;print if $n>1' ${name}.mat >${name}.m
plot-mat.py ${name}.m ${name}-mat.png

diffuse all ${name}.mat ${name}.corr
plot-mat.py ${name}.corr ${name}-corr.png

diffuse ${name}.mat $sites1 ${name}.nodes1 ${name}.bonds1 -n 10000 -c 0.05
diffuse ${name}.mat $sites2 ${name}.nodes2 ${name}.bonds2 -n 10000 -c 0.05

bfactor ${name}.pdb ${name}.nodes1 ${name}-bfactor1.pdb
bfactor ${name}.pdb ${name}.nodes2 ${name}-bfactor2.pdb

plot-nodes.py ${name}.nodes1 ${name}-nodes1.png
plot-nodes.py ${name}.nodes2 ${name}-nodes2.png

diffuse path ${name}.mat $sites1 $sites2 ${name}.paths -n 10000 -c 0.05

plot-paths ${name}-bfactor1.pdb ${name}.paths ${name}1.pym
plot-paths ${name}-bfactor2.pdb ${name}.paths ${name}2.pym

pymol -c ${name}1.pym
pymol -c ${name}2.pym


module load anaconda
module load allos

name=${1}
sites1=$(cat sites1)

contacts ${name}.pdb -oi ${name}.ind -oc ${name}.mat -c 3.4 -a 0.4
python ind2label.py ${name}.ind ${name}.label
perl -lane '$n++;print if $n>1' ${name}.mat >${name}.m
python plot-mat.py ${name}.m ${name}.label ${name}-mat.png

diffuse all ${name}.mat ${name}.corr
python plot-mat.py ${name}.corr ${name}.label ${name}-corr.png

diffuse ${name}.mat $sites1 ${name}.nodes1 ${name}.bonds1 -n 10000 -c 0.05

bfactor ${name}.pdb ${name}.nodes1 ${name}-bfactor1.pdb

python plot-nodes.py ${name}.nodes1 ${name}-nodes1.png


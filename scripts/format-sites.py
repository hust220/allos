#! /bin/env python

import sys
import re

sites_str = sys.argv[1]
ind_file = sys.argv[2]
sites_file = sys.argv[3]

residues = []
for s in re.split(',', sites_str):
  w = re.split('/', s)
  chain_name = w[0]
  u = re.split('\+', w[1])
  for i in u:
    v = re.split('-', i)
    if len(v) == 1:
      residues.append('{0}/{1}'.format(chain_name, v[0]))
    elif len(v) == 2:
      a = int(v[0])
      b = int(v[1])
      for j in range(a, b+1):
        residues.append('{0}/{1}'.format(chain_name, j))

inds = []
for line in open(ind_file):
  l = re.split('\s+', line.strip())
  if len(l) == 3:
    ind = int(l[0])
    chain = l[1]
    residue = l[2]

    w = re.split('/', chain)
    chain_name = w[1]

    w = re.split('/', residue)
    res_num = int(w[0])

    s = '{0}/{1}'.format(chain_name, res_num)

    if s in residues:
      inds.append(ind)

f = open(sites_file, 'w+')
f.write('+'.join([str(i) for i in inds]))
f.close()

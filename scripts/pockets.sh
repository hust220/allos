module load allos
module load anaconda

name=$1

sites1=$(cat sites1)
sites2=$(cat sites2)
exclude ${name}-bfactor1.pdb "$sites1+$sites2" ${name}-bfactor1-main.pdb
include ${name}-bfactor1.pdb "$(cat sites1)" ${name}-bfactor1-sites1.pdb
include ${name}-bfactor1.pdb "$(cat sites2)" ${name}-bfactor1-sites2.pdb
pocket -i ${name}-bfactor1-main.pdb -o ${name}-pockets.pdb -sc 0.3 -int 1 -delta 5 >pockets.txt
bash gen-pocket-pse.sh ${name}-bfactor1-main.pdb ${name}-bfactor1-sites1.pdb ${name}-bfactor1-sites2.pdb ${name}-pockets.pdb


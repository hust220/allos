import sys
import re

pathfile = sys.argv[1]
maxN = sys.argv[2]
outfile = sys.argv[3]

if maxN == 'all':
    considerAll = True
else:
    considerAll = False
    maxN = int(maxN)

nodes = {'Start':0, 'End':0}
edges = set()
nLine = 0

for line in open(pathfile):
    nLine += 1
    if considerAll or nLine <= maxN:
#        print(line)
        l = re.split('\s+', line.strip())
        if len(l) == 2:
            weight = float(l[0])
            path = [int(i) for i in re.split('-', l[1])]
            n = len(path)

            nodes['Start'] += weight
            for i in range(n-2):
                node = path[i+1]
                if node not in nodes:
                    nodes[node] = 0
                nodes[node] += weight
            nodes['End'] += weight

            if n == 2:
                edges.add(('Start', 'End'))
            else:
                edges.add(('Start', path[1]))
                for i in range(n-3):
                    edges.add((path[i+1], path[i+2]))
                edges.add((path[-2], 'End'))

sortedKeys = sorted(nodes, key=nodes.get, reverse=True)

f = open(outfile, 'w+')
f.write('NODES\n')
for k in sortedKeys:
    f.write('{0} {1}\n'.format(k, nodes[k]))

f.write('EDGES\n')
for edge in edges:
    f.write('{0}-{1}\n'.format(edge[0], edge[1]))
f.close()

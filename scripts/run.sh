module load anaconda
module load allos

name=${1}
sites1=$(cat sites1)
sites2=$(cat sites2)

echo "Name: $name"
echo "Sites: $sites1 $sites2"

# Remove all hydrogen atoms
#perl -lane 'if(substr($_,12,4)=~/H/){next}else{print}' ${name}.pdb >${name}-NH.pdb

contacts ${name}.pdb -oi ${name}.ind -oc ${name}.mat -c 3.4 -a 0.45
python ind2label.py ${name}.ind ${name}.label
perl -lane '$n++;print if $n>1' ${name}.mat >${name}.m
python plot-mat.py ${name}.m ${name}.label ${name}-mat.png

diffuse all ${name}.mat ${name}.corr
python plot-mat.py ${name}.corr ${name}.label ${name}-corr.png

diffuse ${name}.mat $sites1 ${name}.nodes1 ${name}.bonds1 -n 10000 -c 0.05
diffuse ${name}.mat $sites2 ${name}.nodes2 ${name}.bonds2 -n 10000 -c 0.05

bfactor ${name}.pdb ${name}.nodes1 ${name}-bfactor1.pdb
bfactor ${name}.pdb ${name}.nodes2 ${name}-bfactor2.pdb

python plot-nodes.py ${name}.nodes1 ${name}-nodes1.png
python plot-nodes.py ${name}.nodes2 ${name}-nodes2.png

diffuse path ${name}.mat $sites1 $sites2 ${name}.paths -n 100 -c 0.05
python get-network.py ${name}.paths 100 ${name}.net

plot-paths -i ${name}-bfactor1.pdb -p ${name}.paths -o ${name}1.pym
plot-paths -i ${name}-bfactor2.pdb -p ${name}.paths -o ${name}2.pym

pymol -c ${name}1.pym
pymol -c ${name}2.pym


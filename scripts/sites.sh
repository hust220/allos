module load anaconda
module load allos

name=${1}
sites1=$(cat sites1)
sites2=$(cat sites2)

echo "Name: $name"
echo "Sites: $sites1 $sites2"

contacts ${name}.pdb -oi ${name}.ind -oc ${name}.mat -c 3.4 -a 3
python ind2label.py ${name}.ind ${name}.label
perl -lane '$n++;print if $n>1' ${name}.mat >${name}.m
python plot-mat.py ${name}.m ${name}.label ${name}-mat.png

diffuse ${name}.mat $sites1 ${name}.nodes1 ${name}.bonds1 -n 10000 -c 0.05
diffuse ${name}.mat $sites2 ${name}.nodes2 ${name}.bonds2 -n 10000 -c 0.05

bfactor ${name}.pdb ${name}.nodes1 ${name}-bfactor1.pdb
bfactor ${name}.pdb ${name}.nodes2 ${name}-bfactor2.pdb

python plot-nodes.py ${name}.nodes1 ${name}-nodes1.png
python plot-nodes.py ${name}.nodes2 ${name}-nodes2.png



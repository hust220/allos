module load anaconda

pdb=$1
sites1=$2
sites2=$3
pocket=$4

fullname=${pocket%.*}

cat <<! >${fullname}.pym
cmd.bg_color('white')
cmd.load("$pdb", 'main')
cmd.load("$sites1", 'sites1')
cmd.load("$sites2", 'sites2')
cmd.load("$pocket", 'pockets')
cmd.zoom("all")

cmd.show_as('surface', 'main')
cmd.show_as('sticks', 'sites1')
cmd.show_as('sticks', 'sites2')
cmd.show_as('spheres', 'pockets')

cmd.spectrum("b",selection="main or sites1 or sites2")
util.color_chains('pockets')

cmd.save("${fullname}.pse")
cmd.quit()
!

pymol -c ${fullname}.pym

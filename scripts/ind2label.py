import sys
import re

indfile = sys.argv[1]
labelfile = sys.argv[2]

f = open(labelfile, 'w+')
for line in open(indfile):
    l = re.split('\s+', line.strip())
    if len(l) == 3:
        chain = re.split('/', l[1])[1]
        res = re.split('/', l[2])[0]
        f.write('{0}-{1}\n'.format(chain, res))
f.close()

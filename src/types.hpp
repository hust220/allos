#pragma once

#include "jnc/bio"
#include "type_aux.hpp"

namespace rnalig {

/**
 * Basic Atom Type.
 */
class BasicAtom : public std::array<double, 3> {
public:
    std::string name;
    int type;
    int element;

    JN_DEFAULTS(BasicAtom);

};

/**
 * Hydrogen.
 */
class Hydrogen : public BasicAtom {
public:
    JN_DEFAULTS(Hydrogen);

    Hydrogen(const jnc::pdb::Atom &atom) {
        for (int i = 0; i < 3; i++) this->at(i) = atom[i];
        name = atom.name;
        element = atom.get_element_type();
    }

    Hydrogen(const jnc::mol2::Atom &atom) {
        this->at(0) = atom.x;
        this->at(1) = atom.y;
        this->at(2) = atom.z;
        name = atom.name;
        element = jnc::mol2::get_atom_element(atom.type);
    }
};

/**
 * Atom.
 */
class Atom : public BasicAtom {
public:
    std::vector<Hydrogen> hydrogens;

    JN_DEFAULTS(Atom);

    Atom(const jnc::pdb::Atom &atom) {
        for (int i = 0; i < 3; i++) this->at(i) = atom[i];
        name = atom.name;
        element = atom.get_element_type();
    }

    Atom(const jnc::mol2::Atom &atom) {
        this->at(0) = atom.x;
        this->at(1) = atom.y;
        this->at(2) = atom.z;
        name = atom.name;
        element = jnc::mol2::get_atom_element(atom.type);
    }

    double &operator [](int i) {
        return this->at(i);
    }

    const double &operator [](int i) const {
        return this->at(i);
    }

    Hydrogen &operator [](const std::string &atom_name) {
        return *find_if(hydrogens.begin(), hydrogens.end(), [&atom_name](const Hydrogen &atom){
            return atom.name == atom_name;
        });
    }

    const Hydrogen &operator [](const std::string &atom_name) const {
        return *find_if(hydrogens.begin(), hydrogens.end(), [&atom_name](const Hydrogen &atom){
            return atom.name == atom_name;
        });
    }
};
using Atoms = std::vector<Atom>;

/**
 * Residue.
 */
class Residue : public Atoms {
public:
    std::string name;

    JN_DEFAULTS(Residue);

    Residue(const jnc::pdb::Residue &residue);

    Atom &operator [](int i) {
        return this->at(i);
    }

    const Atom &operator [](int i) const {
        return this->at(i);
    }

    Atom &operator [](const std::string &atom_name) {
        auto it = find_if(this->begin(), this->end(), [&atom_name](const Atom &atom){
            return atom.name == atom_name;
        });
        if (it == this->end()) throw jnc::string_format("No Atom %s in Residue %s", atom_name, name);
        return *it;
    }

    const Atom &operator [](const std::string &atom_name) const {
         auto it = find_if(this->begin(), this->end(), [&atom_name](const Atom &atom){
            return atom.name == atom_name;
        });
        if (it == this->end()) throw jnc::string_format("No Atom %s in Residue %s", atom_name, name);
        return *it;
    }

    void add_hydrogens();

    void remove_hydrogens();
};
using Residues = std::vector<Residue>;

/**
 * Bond.
 */
class Bond : public std::array<int, 3> {
public:
    JN_DEFAULTS(Bond);

    Bond(const jnc::mol2::Bond &bond) {
        this->at(0) = bond.origin_atom_id;
        this->at(1) = bond.target_atom_id;
        this->at(2) = jnc::mol2::BondType::get_id(bond.type);
    }
};
using Bonds = std::vector<Bond>;

/**
 * Receptor.
 */
class Receptor : public Residues {
public:
    std::string name;

    JN_DEFAULTS(Receptor);

    Receptor(const jnc::pdb::Chain &chain) {
        name = chain.name;
        for (auto && residue : chain) {
            push_back(residue);
        }
    }

    Receptor(const jnc::pdb::Model &model) {
        name = model.name;
        for (auto && chain : model) {
            for (auto && residue : chain) {
                push_back(residue);
            }
        }
    }

    Receptor(const jnc::pdb::Pdb &m) {
        name = m.name;
        for (auto && chain : m[0]) {
            for (auto && residue : chain) {
                push_back(residue);
            }
        }
    }

    Receptor(const std::string &filename) : Receptor(jnc::pdb::Pdb(filename)) {}

    int num_atoms() const {
        int i = 0;
        for (auto && residue : *this) {
            for (auto && atom : residue) {
                i++;
            }
        }
        return i;
    }

    void add_hydrogens() {
        for (auto && res : *this) {
            res.add_hydrogens();
        }
    }

    void remove_hydrogens() {
        for (auto && res : *this) {
            res.remove_hydrogens();
        }
    }
};

/**
 * Ligand.
 */
class Ligand : public Residue {
public:
    std::string name;
    Bonds bonds;

    JN_DEFAULTS(Ligand);

    Ligand(const jnc::pdb::Pdb &s) : Residue(s[0][0][0]) {}

    Ligand(const jnc::pdb::Model &model) : Residue(model[0][0]) {}

    Ligand(const jnc::pdb::Chain &chain) : Residue(chain[0]) {}

    Ligand(const jnc::pdb::Residue &residue) : Residue(residue) {}

    Ligand(const jnc::mol2::Mol2 &mol) {
        init(mol);
    }

    Ligand(const std::string &filename) {
        if (jnc::string_ends_with(filename, ".pdb")) {
            *this = Ligand(jnc::pdb::Pdb(filename));
        }
        else if (jnc::string_ends_with(filename, ".mol2")) {
            init(jnc::mol2::Mol2(filename));
        }
    }

    int num_atoms() const {
        return size();
    }

    void add_hydrogens();

    void remove_hydrogens();

//    void reset_atom_types();

private:
    void init(const jnc::mol2::Mol2 &mol_);
};

} // namespace jnc


#pragma once

#include <deque>
#include <algorithm>
#include <vector>
#include <utility>
#include <numeric>
#include <random>
#include "mat.hpp"
#include "../core/opt.hpp"

namespace jnc {

class Kmeans {
public:
    using cluster_t = std::vector<int>;
    using clusters_t = std::vector<cluster_t>;

    int k = 1;
    int max_steps = 200;

    clusters_t clusters;

    void rand_select(int size) {
        std::vector<int> a(size);
        std::random_device rd;
        std::mt19937 gen(rd());

        if (size < k) throw "The number of objects to be clustered should be not less than the number of clusters.";

        std::iota(a.begin(), a.end(), 0);
        for (int i = 0; i < k; i++) {
            std::uniform_int_distribution<> dis(0, a.size() - 1);
            int n = dis(gen);
            clusters[i].push_back(a[n]);
            a[n] = a.back();
            a.pop_back();
        }
    }

    template<typename RandomIt, typename F>
    void run(RandomIt && first, RandomIt && last, F && dist) {
        Mat *m = to_mat(first, last, dist);
        run(*m);
        delete m;
    }

    template<typename RandomIt, typename F>
    static Mat *to_mat(RandomIt && first, RandomIt && last, F && dist) {
        int i, j, size;
        Num d;
        Mat *mat;

        size = std::distance(first, last);
        mat = new Mat(size, size);
        for (auto it1 = first; it1 != last; it1++) {
            for (auto it2 = it1; it2 != last; it2++) {
                i = std::distance(first, it1);
                j = std::distance(first, it2);
                if (i == j) {
                    (*mat)(i, j) = 0;
                }
                else {
                    d = dist(*it1, *it2);
                    (*mat)(i, j) = (*mat)(j, i) = d;
                }
            }
        }
        return mat;
    }

    void run(const Mat &mat) {
        // Clear clusters
        clusters.clear();
        clusters.resize(k);

        // select k points randomly
        rand_select(mat.rows());

        int flag = 1;
        int n = max_steps;
        while (n >= 1 && flag != 0) {
            n--;
            flag = 0;
            /// classify
            for (int i = 0; i < mat.rows(); i++) {
                if (std::any_of(clusters.begin(), clusters.end(), [&](auto &&v) {return v[0] == i; }))
                    continue;
                auto min_elem = std::min_element(clusters.begin(), clusters.end(), [&](auto &&v1, auto &&v2) {
                    Num d1 = mat(i, v1[0]);
                    Num d2 = mat(i, v2[0]);
                    return d1 < d2;
                });
                min_elem->push_back(i);
            }

            /// select new center
            for (auto &&cluster : clusters) {
                std::vector<Num> costs(cluster.size());
                std::transform(cluster.begin(), cluster.end(), costs.begin(), [&](int i) {
                    return std::accumulate(cluster.begin(), cluster.end(), 0.0, [&](Num sum, int j)->Num {
                        return sum + mat(i, j);
                    });
                });
                auto min_elem = std::min_element(costs.begin(), costs.end());
                if (min_elem != costs.begin()) {
                    flag++;
                    std::swap(cluster[0], cluster[std::distance(costs.begin(), min_elem)]);
                }
            }
            if (flag == 0 || n == 0) {
            }
            else {
                for (auto &&cluster : clusters)
                    cluster.resize(1);
            }
        }

        std::sort(clusters.begin(), clusters.end(), [](auto &&c1, auto &&c2) {return c1.size() > c2.size(); });
    }
};

}


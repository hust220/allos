#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <list>
#include <deque>
#include <array>
#include "Eigen/Dense"
#include "Eigen/Core"
#include "Eigen/SVD"
#include "Eigen/Geometry"
#include "../core/traits.hpp"


namespace jnc {

template<typename NumType>
using MatX = Eigen::Matrix<NumType, -1, -1>;
using Mat = MatX<Num>;
using Matf = MatX<float>;
using Matd = MatX<double>;
using Mati = MatX<int>;
using Matu = MatX<unsigned>;

template<typename NumType>
using VecX = Eigen::Matrix<NumType, -1, 1>;
using Vec = VecX<Num>;
using Vecf = VecX<float>;
using Vecd = VecX<double>;
using Veci = VecX<int>;
using Vecu = VecX<unsigned>;

template<typename NumType>
using RowVecX = Eigen::Matrix<NumType, 1, -1>;
using RowVec = RowVecX<Num>;
using RowVecf = RowVecX<float>;
using RowVecd = RowVecX<double>;
using RowVeci = RowVecX<int>;
using RowVecu = RowVecX<unsigned>;

template<typename _Row>
inline void mat_set_rows(Mat &mat, int beg, const _Row &row) {
    for (int i = 0; i < 3; i++) {
        mat(beg, i) = row[i];
    }
}

template<typename _First, typename _Second, typename... _Rest>
inline void mat_set_rows(Mat &mat, int beg, const _First &first, const _Second &second, const _Rest &...rest) {
    mat_set_rows(mat, beg, first);
    mat_set_rows(mat, beg + 1, second, rest...);
}

template<typename _Vec, typename _Row>
inline void vec_set(_Vec &vec, const _Row &row) {
    for (int i = 0; i < 3; i++) vec[i] = row[i];
}

template<typename _Vec, typename _Fn, typename _First, typename... _Rest>
inline void vec_set(_Vec &vec, _Fn &&fn, const _First &first, const _Rest &...rest) {
    for (int i = 0; i < 3; i++) vec[i] = fn(first[i], (rest[i])...);
}

} //namespace jnc


#pragma once

#include <vector>
#include "mat.hpp"

namespace jnc {

std::vector<int> kmeans(const Mat &m, int k);

}


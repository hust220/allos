#include <iostream>
#include <cctype>

#include "string.hpp"
#include "opt.hpp"

namespace jnc {

Opt::Opt(int argc, char **argv) {
    read(argc, argv);
}

Opt::Opt(Str str) {
    read(str);
}

void Opt::read(Str par_file) {
    STD_ ifstream ifile(par_file.c_str());
    Str line;
    while (ifile) {
        STD_ getline(ifile, line);
        auto &&tokens = string_tokenize(line, " ");
        if (!tokens.empty()) {
            pars[tokens[0]] = Qs{};
            for (auto it2 = STD_ next(tokens.begin()); it2 != tokens.end(); it2++) {
                pars[tokens[0]].push_back(*it2);
            }
            if (tokens[0] == "par") {
                for (auto && p : pars["par"]) read(p);
            }
        }
    }
    ifile.close();

}

void Opt::read(int argc, char **argv) {
    this->argc = argc;
    this->argv = argv;
    Str key;
    Qs values;
    int n = 0;
    for (int i = 1; i < argc; i++) {
        if (Str(argv[i]).size() >=2 && argv[i][0] == '-' && STD_ isalpha(argv[i][1])) {
            if (n != 0) {
                pars[key] = values;
                if (key == "par") {
                    for (auto && p : values) {
                        read(p);
                    }
                }
            } else {
                g = values;
                //pars["global"] = values;
            }
            Str str(argv[i]);
            key = str.substr(1, str.size() - 1);
            values.clear();
            n++;
        } else {
            values.push_back(argv[i]);
        }
    }
    if (n != 0) {
        pars[key] = values;
        if (key == "par") {
            for (auto && p : values) read(p);
        }
    } else {
        g = values;
        //pars["global"] = values;
    }
}

STD_ ostream &operator <<(STD_ ostream &out, const Opt &par) {
    for (auto && i : par.pars) {
        out << i.first;
        for (auto && j : i.second) {
            out << ' ' << j;
        }
        out << STD_ endl;
    }
    return out;
}

} // namespace jnc


/***************************************************************************
 *
 * Authors: "Jian Wang"
 * Email: jianopt@163.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This complete copyright notice must be included in any revised version of the
 * source code. Additional authorship citations may be added, but existing
 * author citations must be preserved.
 ***************************************************************************/

#pragma once

#include <chrono>
#include <cmath>
#include <utility>

namespace jnc {

/**
 * Square
 */
double square(double n);

/**
 * Return a random floating number between 0 and 1.
 */
double rand();

/*
 * Set the seed of the random engine
 */
void seed(unsigned t);

template<typename F, typename T1>
inline auto sum(F &&f, T1 &&t1) -> decltype(f(t1)) {
    return f(std::forward<T1>(t1));
}

template<typename F, typename T1, typename... T2>
inline auto sum(F &&f, T1 &&t1, T2 && ...t2) -> decltype(f(t1)) {
    return f(std::forward<T1>(t1)) + sum(f, std::forward<T2>(t2)...);
}

} // namespace jnc


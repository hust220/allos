/***************************************************************************
 *
 * Authors: "Jian Wang"
 * Email: jianopt@163.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This complete copyright notice must be included in any revised version of the
 * source code. Additional authorship citations may be added, but existing
 * author citations must be preserved.
 ***************************************************************************/

#pragma once

#include <type_traits>
#include <functional>
#include <vector>
#include <list>
#include <deque>
#include <map>
#include <utility>
#include <memory>
#include <set>
#include <tuple>
#include <fstream>
#include <sstream>
#include <iostream>
#include <algorithm>

#include "macros.hpp"
#include "platform.hpp"

namespace jnc {

#define JN_ENABLE(_cond) typename ::std::enable_if<_cond, int>::type = 1
#define JN_IS_SAME(A_, B_) ::std::is_same<A_, B_>::value

template<bool Condition, typename Type>
using jn_enable_t = typename ::std::enable_if<Condition, Type>::type;

template<bool A, bool B> struct jn_static_or                 { enum { value = true  }; };
template<>               struct jn_static_or<false, false>   { enum { value = false }; };
#define JN_STATIC_OR(A, B)   jn_static_or<A, B>::value

template<bool A, bool B> struct jn_static_and                { enum { value = false }; };
template<>               struct jn_static_and<true, true>    { enum { value = true  }; }; 
#define JN_STATIC_AND(A, B)  jn_static_and<A, B>::value

template<bool A>         struct jn_static_not                { enum { value = false }; };
template<>               struct jn_static_not<false>         { enum { value = true  }; }; 
#define JN_STATIC_NOT(A)     jn_static_not<A>::value

/**
 * Calculate the number of types.
 */
template<typename _T, typename... _Types>
struct jn_num_types {
    enum { N = 1 + jn_num_types<_Types...>::N };
};

template<typename _T>
struct jn_num_types<_T> { enum { N = 1 }; };

/**
 * Retrieve the first type.
 */
template<typename _T, typename... _Types>
struct jn_first {
    using type = _T;
};

template<typename... _Types>
using jn_first_t = typename jn_first<_Types...>::type;

#ifdef JN_PRECISION
using Num = JN_PRECISION;
#else
using Num = double;
#endif

/**
 * Definition of jn_if.
 */
template<bool condition, typename T, typename F> struct jn_if {
    using type = T;
};

template<typename T, typename F>
struct jn_if<false, T, F> {
    using type = F;
};

template<bool condition, typename T, typename F>
using jn_if_t = typename jn_if<condition, T, F>::type;

template<typename _CharType, typename _CharTraits = STD_ char_traits<_CharType>>
using BStr = STD_ basic_string<_CharType, _CharTraits>;
using Str = STD_ string;
using WStr = STD_ wstring;

template<typename _Type>
using Ptr = _Type *;

template<typename _Type>
using SPtr = STD_ shared_ptr<_Type>;
template<typename _Type>
using SP = SPtr<_Type>;

template<typename _Type>
using UPtr = STD_ unique_ptr<_Type>;
template<typename _Type>
using UP = UPtr<_Type>;

template<typename _Fty>
using Fn = STD_ function<_Fty>;
template<typename _Fty>
using Function = STD_ function<_Fty>;

template<typename _Type, int N_>
using A = STD_ array<_Type, N_>;
template<int N_> using An = A<Num, N_>;
template<int N_> using Ai = A<int, N_>;
template<int N_> using Ad = A<double, N_>;
template<int N_> using Af = A<float, N_>;
template<int N_> using Ac = A<char, N_>;
template<int N_> using As = A<Str, N_>;
template<int N_> using Ab = A<bool, N_>;

template<typename _Type>
using V = STD_ vector<_Type>;
using Vn = V<Num>;
using Vi = V<int>;
using Vd = V<double>;
using Vf = V<float>;
using Vc = V<char>;
using Vs = V<Str>;
using Vb = V<bool>;

template<typename _Type>
using L = STD_ list<_Type>;
using Ln = L<Num>;
using Li = L<int>;
using Ld = L<double>;
using Lf = L<float>;
using Lc = L<char>;
using Ls = L<Str>;
using Lb = L<bool>;

template<typename _Type>
using Q = STD_ deque<_Type>;
using Qn = Q<Num>;
using Qi = Q<int>;
using Qd = Q<double>;
using Qf = Q<float>;
using Qc = Q<char>;
using Qs = Q<Str>;
using Qb = Q<bool>;

template<typename _Type>
using T = STD_ set<_Type>;
using Tn = T<Num>;
using Ti = T<int>;
using Td = T<double>;
using Tf = T<float>;
using Tc = T<char>;
using Ts = T<Str>;
using Tb = T<bool>;

template<typename _Type1, typename _Type2>
using Pr = STD_ pair<_Type1, _Type2>;

template<typename... _Types>
using Tp = STD_ tuple<_Types...>;

template<typename _KeyType, typename _Type>
using M = STD_ map<_KeyType, _Type>;
template<typename _Type>
using Mn = M<Num, _Type>;
template<typename _Type>
using Mi = M<int, _Type>;
template<typename _Type>
using Md = M<double, _Type>;
template<typename _Type>
using Mf = M<float, _Type>;
template<typename _Type>
using Mc = M<char, _Type>;
template<typename _Type>
using Ms = M<Str, _Type>;
template<typename _Type>
using Mb = M<bool, _Type>;

template<typename T>
using Ref = std::reference_wrapper<T>;

template<typename T, typename U>
T lexical_cast(U && u) {
    ::std::stringstream stream;
    T t;

    stream << u;
    stream >> t;
    return t;
}

#define JN_INT(a) ::jnc::lexical_cast<int>(a)
#define JN_DBL(a) ::jnc::lexical_cast<double>(a)
#define JN_FLT(a) ::jnc::lexical_cast<float>(a)
#define JN_STR(a) ::jnc::lexical_cast<Str>(a)

template<typename Stream_>
void stream_push(Stream_ && stream) {
}

template<typename Stream_, typename Value_, typename... Values_>
void stream_push(Stream_ && stream, Value_ && value, Values_ && ...values) {
    stream << value;
    stream_push(stream, values...);
}

template<typename List1, typename List2>
bool eles_equal(const List1 &l1, const List2 &l2) {
    auto it1 = l1.begin();
    auto it2 = l2.begin();
    for (; it1 != l1.end() && it2 != l2.end(); it1++, it2++) {
        if (*it1 != *it2) return false;
    }
    if (it1 != l1.end() || it2 != l2.end()) return false;
    return true;
}

template<typename LS1_, typename LS2_>
void stl_extend(LS1_ &&ls1, LS2_ &&ls2) {
    for (auto && i : ls2) {
        ls1.push_back(i);
    }
}

} // namespace jnc

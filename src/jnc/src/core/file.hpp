/***************************************************************************
 *
 * Authors: "Jian Wang"
 * Email: jianopt@163.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This complete copyright notice must be included in any revised version of the
 * source code. Additional authorship citations may be added, but existing
 * author citations must be preserved.
 ***************************************************************************/

#pragma once

#include <string>
#include <sstream>

#include "string.hpp"
#include "platform.hpp"

namespace jnc {

enum {
    JN_CONTINUE,
    JN_BREAK,
    JN_GO
};

template<typename Func_>
void file_each_line(Str fn, Func_ &&f) {
    std::ifstream ifile(fn.c_str());
    Str line;
    while (ifile) {
        std::getline(ifile, line);
        auto r = f(line);
        if (r == JN_CONTINUE)  continue;
        else if (r == JN_BREAK) break;
    }
    ifile.close();
}

} // namespace jnc



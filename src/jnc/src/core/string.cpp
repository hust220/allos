#include "string.hpp"

namespace jnc {

Vs string_tokenize(const Str &str, const Str &delimiters) {
    Vs tokens;
    auto lastPos = str.find_first_not_of(delimiters, 0);
    auto pos = str.find_first_of(delimiters, lastPos);
    while (Str::npos != pos || Str::npos != lastPos) {
        tokens.push_back(str.substr(lastPos, pos - lastPos));
        lastPos = str.find_first_not_of(delimiters, pos);
        pos = str.find_first_of(delimiters, lastPos);
    }
    return std::move(tokens);
}

Vs string_tokenize(const Str &str, const Str &delimiters, const Str &temp) {
    Vs tokens;
    using pair_t = ::std::pair<Str::size_type, Str::size_type>;
    ::std::vector<pair_t> vec;
    Str::size_type first_i, first_j, second_i, second_j;
    int expected = 0;
    for (Str::size_type i = 0; i < str.size(); i++) {
        int flag = 0;
        Str::size_type j;
        for (j = 0; j < temp.size(); j++) {
            if (str[i] == temp[j]) {
                if (j % 2 == 0 && expected == 0) { flag = 1; break;
                } else if (j % 2 == 1 && expected == 1) { flag = 2; break; }
            }
        }
        if (flag == 1) {
            first_i = i; first_j = j; expected = 1;
        } else if (flag == 2 && j - first_j == 1) {
            second_i = i; second_j = j; expected = 0;
            vec.push_back(::std::make_pair(first_i, second_i));
        }
    }
    auto lastPos = str.find_first_not_of(delimiters, 0);
    auto pos = str.find_first_of(delimiters, lastPos);
    while (::std::any_of(vec.begin(), vec.end(), [&pos](const pair_t &p){
        return pos != Str::npos && p.first < pos && pos < p.second;
    })) {
        pos = str.find_first_of(delimiters, pos + 1);
    }
    while (Str::npos != pos || Str::npos != lastPos) {
        tokens.push_back(str.substr(lastPos, pos - lastPos));
        lastPos = str.find_first_not_of(delimiters, pos);
        pos = str.find_first_of(delimiters, lastPos);
        while (::std::any_of(vec.begin(), vec.end(), [&pos](const pair_t &p){
            return pos != Str::npos && p.first < pos && pos < p.second;
        })) {
            pos = str.find_first_of(delimiters, pos + 1);
        }
    }
    return std::move(tokens);
}

void string_upper(Str &s) {
    std::transform(s.begin(), s.end(), s.begin(), ::toupper);
}

Str string_upper_copy(const Str &str) {
    Str s = str; 
    std::transform(s.begin(), s.end(), s.begin(), ::toupper);
    return s;
}

void string_lower(Str &s) {
    std::transform(s.begin(), s.end(), s.begin(), ::tolower);
}

Str string_lower_copy(const Str &str) {
    Str s = str; 
    std::transform(s.begin(), s.end(), s.begin(), ::tolower);
    return s;
}

bool string_includes(const Str &s1, const Str &s2) {
    return s1.find(s2) != std::string::npos;
}

void string_trim(Str &s) {
    if (!(s.empty())) {  
        s.erase(0, s.find_first_not_of(" "));  
        s.erase(s.find_last_not_of(" ") + 1);  
    }
}  

Str string_trim_c(const Str &s) {
    if (s.empty()) {
        return s;
    }
    Str::size_type beg = s.find_first_not_of(" \t\n\r");
    Str::size_type end = s.find_last_not_of(" \t\n\r");
    if (beg == Str::npos || end == Str::npos) {
        return "";
    } else {
        return Str(s.begin() + beg, s.begin() + end + 1);
    }
}

bool string_starts_with(const Str &str1, const Str &str2) {
    int sz = str2.size();
    for (int i = 0; i < sz; i++) {
        if (str1[i] != str2[i]) return false;
    }
    return true;
}

bool string_ends_with(const Str &str1, const Str &str2) {
    int sz = str2.size();
    auto it1 = str1.rbegin();
    auto it2 = str2.rbegin();
    for (int i = 0; i < sz; i++) {
        if (*it1 != *it2) return false;
        it1++;
        it2++;
    }
    return true;
}

} // namespace jnc


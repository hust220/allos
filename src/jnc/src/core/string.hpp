/***************************************************************************
 *
 * Authors: "Jian Wang"
 * Email: jianopt@163.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This complete copyright notice must be included in any revised version of the
 * source code. Additional authorship citations may be added, but existing
 * author citations must be preserved.
 ***************************************************************************/

#pragma once

#include <string>
#include <sstream>
#include <regex>

#include "traits.hpp"

namespace jnc {

Vs string_tokenize(const Str &str, const Str &delimiters = " ");
Vs string_tokenize(const Str &str, const Str &delimiters, const Str &temp);

template<typename... Strs_>
Str string_merge(Strs_ && ...strs) {
    STD_ stringstream stream;
    stream_push(stream, strs...);
    return stream.str();
}

///////////////// string_format //////////////////
template<typename T>
T string_to_chars(T &&t) {
    return t;
}

inline const char *string_to_chars(std::string &s) {
    return s.c_str();
}

inline const char *string_to_chars(const std::string &s) {
    return s.c_str();
}

inline const char *string_to_chars(std::string &&s) {
    return s.c_str();
}

template<typename... Pars_>
Str string_format(std::string fmt, Pars_ && ...pars) {
    int count = snprintf(NULL, 0, fmt.c_str(), string_to_chars(std::forward<Pars_>(pars))...);
    Str buf;
    buf.resize(count);
    sprintf(&(buf[0]), fmt.c_str(), string_to_chars(pars)...);
    return std::move(buf);
}
//////////////////////////////////////////////////

template<typename _Interval, typename _Ls>
static Str string_join(_Interval && interval, _Ls && ls) {
    STD_ stringstream stream;
    int i = 0;
    for (const auto & s : ls) {
        if (i != 0) stream << interval;
        stream << s;
        i++;
    }
    return stream.str();
}

bool string_includes(const Str &str1, const Str &str2);

void string_trim(Str &str);

Str string_trim_c(const Str &str);

bool string_starts_with(const Str &str1, const Str &str2);

bool string_ends_with(const Str &str1, const Str &str2);

void string_upper(Str &str);
Str string_upper_copy(const Str &str);

void string_lower(Str &str);
Str string_lower_copy(const Str &str);

} // namespace jnc

#define JN_DIE(...) do {\
    ::std::cerr << ::jnc::string_merge(__FILE__, "(", __LINE__, "): ", __VA_ARGS__) << ::std::endl;\
    ::std::abort();\
} while(0)

#define JN_ASSERT(condition,what) if(!condition){JN_DIE(what);}



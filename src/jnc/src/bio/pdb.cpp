#include <iomanip>
#include <cctype>
#include "pdb.hpp"
#include "pdb_reader.hpp"
#include "pdb_writer.hpp"

namespace jnc {

namespace pdb {

Pdb::Pdb(const std::string &filename) {
    read(filename);
}

void Pdb::read(const std::string &filename) {
    // Clear the Pdb
    clear();

    PdbReader pdb_reader(*this);
    pdb_reader.read(filename);
}

void Pdb::remove_hydrogens() {
    for (auto && model : *this) {
        for (auto && chain : model) {
            for (auto && res : chain) {
                auto r = res;
                r.clear();
                for (auto && atom : res) {
//                    if (atom.name[0] != 'H' && (!std::isdigit(atom.name[0]) || atom.name[1] != 'H')) {
                    if (atom.name[0] != 'H' && atom.name[1] != 'H') {
                        r.push_back(atom);
                    }
                }
                res = std::move(r);
            }
        }
    }
}

void Pdb::sort() {
    for (auto && model : *this) {
        for (auto && chain : model) {
            for (auto && res : chain) {
                std::sort(res.begin(), res.end(), [](const Atom &a1, const Atom &a2){
                    return a1.name < a2.name;
                });
            }
        }
    }
}

std::ostream &operator <<(std::ostream &output, const Pdb &pdb) {
    PdbWriter pdb_writer(output);
    pdb_writer.write_pdb(pdb);
    return output;
}

} // namespace pdb

} // namespace jnc


#pragma once

#include "../core/traits.hpp"
#include <string>

namespace jnc {

enum {
    MOL_PRO,
    MOL_RNA,
    MOL_DNA
};

#define JN_BIO_ATOM_TABLE \
    ELT("H" , ATOM_ELEMENT_H ) SEP \
    ELT("C" , ATOM_ELEMENT_C ) SEP \
    ELT("N" , ATOM_ELEMENT_N ) SEP \
    ELT("O" , ATOM_ELEMENT_O ) SEP \
    ELT("S" , ATOM_ELEMENT_S ) SEP \
    ELT("P" , ATOM_ELEMENT_P ) SEP \
    ELT("K" , ATOM_ELEMENT_K ) SEP \
    ELT("F" , ATOM_ELEMENT_F ) SEP \
    ELT("I" , ATOM_ELEMENT_I ) SEP \
    ELT("LP", ATOM_ELEMENT_LP) SEP \
    ELT("MG", ATOM_ELEMENT_MG) SEP \
    ELT("CR", ATOM_ELEMENT_CR) SEP \
    ELT("CO", ATOM_ELEMENT_CO) SEP \
    ELT("CL", ATOM_ELEMENT_CL) SEP \
    ELT("SE", ATOM_ELEMENT_SE) SEP \
    ELT("NA", ATOM_ELEMENT_NA) SEP \
    ELT("SI", ATOM_ELEMENT_SI) SEP \
    ELT("FE", ATOM_ELEMENT_FE) SEP \
    ELT("ZN", ATOM_ELEMENT_ZN) SEP \
    ELT("SN", ATOM_ELEMENT_SN) SEP \
    ELT("LI", ATOM_ELEMENT_LI) SEP \
    ELT("AL", ATOM_ELEMENT_AL) SEP \
    ELT("CA", ATOM_ELEMENT_CA) SEP \
    ELT("MN", ATOM_ELEMENT_MN) SEP \
    ELT("CU", ATOM_ELEMENT_CU) SEP \
    ELT("BR", ATOM_ELEMENT_BR) SEP \
    ELT("MO", ATOM_ELEMENT_MO)

#define ELT(atom_name, atom_element) atom_element
#define SEP ,
enum { JN_BIO_ATOM_TABLE , ATOM_ELEMENT_X };
#undef SEP
#undef ELT

int get_atom_element(const std::string &atom_name);

std::string get_element_name(int element);

/*

namespace pdb {

enum {
    ATOM_TYPE_P,
    ATOM_TYPE_C5_,
    ATOM_TYPE_O5_,
    ATOM_TYPE_C4_,
    ATOM_TYPE_O4_,
    ATOM_TYPE_C3_,
    ATOM_TYPE_O3_,
    ATOM_TYPE_C2_,
    ATOM_TYPE_O2_,
    ATOM_TYPE_C1_,
    ATOM_TYPE_OP1,
    ATOM_TYPE_OP2,

    ATOM_TYPE_H1_,
    ATOM_TYPE_H2_,
    ATOM_TYPE_H3_,
    ATOM_TYPE_H4_,
    ATOM_TYPE_H5_,
    ATOM_TYPE_H5__,
    ATOM_TYPE_HO2_,

    ATOM_TYPE_N1,
    ATOM_TYPE_C2,
    ATOM_TYPE_O2,
    ATOM_TYPE_N2,
    ATOM_TYPE_N3,
    ATOM_TYPE_C4,
    ATOM_TYPE_O4,
    ATOM_TYPE_N4,
    ATOM_TYPE_C5,
    ATOM_TYPE_C6,
    ATOM_TYPE_N6,
    ATOM_TYPE_O6,
    ATOM_TYPE_N7,
    ATOM_TYPE_C8,
    ATOM_TYPE_N9,

    ATOM_TYPE_H1,
    ATOM_TYPE_H2,
    ATOM_TYPE_H21,
    ATOM_TYPE_H22,
    ATOM_TYPE_H3,
    ATOM_TYPE_H41,
    ATOM_TYPE_H42,
    ATOM_TYPE_H5,
    ATOM_TYPE_H6,
    ATOM_TYPE_H61,
    ATOM_TYPE_H62,
    ATOM_TYPE_H8
};

} // namespace pdb

namespace mol2 {

enum {
    ATOM_TYPE_H,
    ATOM_TYPE_H_SPC,
    ATOM_TYPE_H_T3P,

    ATOM_TYPE_C_1,
    ATOM_TYPE_C_2,
    ATOM_TYPE_C_3,
    ATOM_TYPE_C_AR,
    ATOM_TYPE_C_CAT,

    ATOM_TYPE_N_1,
    ATOM_TYPE_N_2,
    ATOM_TYPE_N_3,
    ATOM_TYPE_N_4,
    ATOM_TYPE_N_AR,
    ATOM_TYPE_N_AM,
    ATOM_TYPE_N_PL3,

    ATOM_TYPE_O_2,
    ATOM_TYPE_O_3,
    ATOM_TYPE_O_CO2,
    ATOM_TYPE_O_T3P,
    ATOM_TYPE_O_SPC,

    ATOM_TYPE_P_3,

    ATOM_TYPE_S_2,
    ATOM_TYPE_S_3,
    ATOM_TYPE_S_O,
    ATOM_TYPE_S_O2,

    ATOM_TYPE_LP,
    ATOM_TYPE_HEV,
    ATOM_TYPE_MG,
    ATOM_TYPE_K,
    ATOM_TYPE_CR_OH,
    ATOM_TYPE_CO_OH,
    ATOM_TYPE_CL,
    ATOM_TYPE_SE,
    ATOM_TYPE_DU_C,
    ATOM_TYPE_HET,
    ATOM_TYPE_NA,
    ATOM_TYPE_SI,
    ATOM_TYPE_CR_TH,
    ATOM_TYPE_FE,
    ATOM_TYPE_F,
    ATOM_TYPE_ZN,
    ATOM_TYPE_I,
    ATOM_TYPE_SN,
    ATOM_TYPE_DU,
    ATOM_TYPE_HAL,
    ATOM_TYPE_LI,
    ATOM_TYPE_AL,
    ATOM_TYPE_CA,
    ATOM_TYPE_MN,
    ATOM_TYPE_CU,
    ATOM_TYPE_BR,
    ATOM_TYPE_MO,
    ATOM_TYPE_ANY
};

} // namespace mol2

*/

} // namespace jnc


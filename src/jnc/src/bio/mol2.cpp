#include "jnc/core"
#include "mol2.hpp"

namespace jnc {

namespace mol2 {

Mol2::Mol2(const std::string &filename) {
    read(filename);
}

void Mol2::remove_hydrogens() {
    Atoms as;
    Vi vi(atoms.size(), -1);
    int i = 0;
    int j = 0;
    for (auto &&atom : atoms) {
        auto &type = atom.type;
        if (type != "H" && type.find("H.") == std::string::npos) {
            vi[i] = j;
            as.push_back(atom);
            j++;
        }
        i++;
    }
    atoms = std::move(as);

    Bonds bs;
    for (auto &&bond : bonds) {
        bond.origin_atom_id = vi[bond.origin_atom_id];
        bond.target_atom_id = vi[bond.target_atom_id];
        if (bond.origin_atom_id != -1 && bond.target_atom_id != -1) bs.push_back(bond);
    }
    bonds = std::move(bs);

    num_atoms = atoms.size();
    num_bonds = bonds.size();
}

std::vector<Mol2> Mol2::read_multiple(const std::string &fn) {
    Str section;
    std::vector<Mol2> ms;
    Mol2 *p = NULL;

    int mol_lines = 0;
    file_each_line(fn, [&section, &mol_lines, &p, &ms](Str &line){
        if (!line.compare(0, 9, "@<TRIPOS>")) {
            section = line.substr(9);
            if (section == "MOLECULE") {
                ms.push_back(Mol2{});
                p = &(ms.back());
            }
        }
        else if (string_tokenize(line, " \t").empty()) {
        }
        else {
            if (section == "MOLECULE") {
                mol_lines++;
                if (mol_lines == 1) p->mol_name = string_trim_c(line);
                else if (mol_lines == 2) {
                    auto &&vs = string_tokenize(line, " \t");
                    if (vs.size() >= 1) p->num_atoms = JN_INT(vs[0]);
                    if (vs.size() >= 2) p->num_bonds = JN_INT(vs[1]);
                    if (vs.size() >= 3) p->num_subst = JN_INT(vs[2]);
                    if (vs.size() >= 4) p->num_feat  = JN_INT(vs[3]);
                    if (vs.size() >= 5) p->num_sets  = JN_INT(vs[4]);
                }
                else if (mol_lines == 3) {
                    p->mol_type = string_trim_c(line);
                }
                else if (mol_lines == 4) p->charge_type = string_trim_c(line);
                else if (mol_lines == 5) p->status_bits = string_trim_c(line);
                else if (mol_lines == 6) p->mol_comment = string_trim_c(line);
            }
            else if (section == "ATOM") {
                auto &&vs = string_tokenize(line, " \t");
                Atom atom;
                if (vs.size() >= 6) {
                    atom.name = vs[1];
                    atom.x = JN_DBL(vs[2]);
                    atom.y = JN_DBL(vs[3]);
                    atom.z = JN_DBL(vs[4]);
                    atom.type = vs[5];
                    if (vs.size() >= 7) atom.subst_id = JN_INT(vs[6])-1;
                    if (vs.size() >= 8) atom.subst_name = vs[7];
                    if (vs.size() >= 9) atom.charge = JN_DBL(vs[8]);
                    if (vs.size() >= 10) atom.status_bit = vs[9];

                    p->atoms.push_back(atom);
                }
                else {
                    JN_DIE("Wrong mol2 format!");
                }
            }
            else if (section == "BOND") {
                auto &&vs = string_tokenize(line, " \t");
                if (vs.size() >= 4) {
                    Bond bond;
                    bond.origin_atom_id = JN_INT(vs[1])-1;
                    bond.target_atom_id = JN_INT(vs[2])-1;
                    bond.type = vs[3];
                    if (vs.size() > 4) {
                        bond.status_bits  = vs[4];
                    }
                    p->bonds.push_back(bond);
                }
            }
            else if (section == "SUBSTRUCTURE") {
                auto &&vs = string_tokenize(line, " \t");
                if (vs.size() >= 3) {
                    Substructure subst;
                    subst.name = vs[1];
                    subst.root_atom = JN_INT(vs[2]);
                    if (vs.size() > 3) subst.type = vs[3];
                    if (vs.size() > 4) subst.dict_type = JN_INT(vs[4]);
                    if (vs.size() > 5) subst.chain = vs[5];
                    if (vs.size() > 6) subst.sub_type = vs[6];
                    if (vs.size() > 7) subst.inter_bonds = JN_INT(vs[7]);
                    if (vs.size() > 8) subst.status = vs[8];
                    if (vs.size() > 9) subst.comment = vs[9];

                    p->substructures.push_back(subst);
                }
            }
        }
        return JN_GO;
    });
    return std::move(ms);
}

void Mol2::read(const std::string &fn) {
    auto &&ms = read_multiple(fn);
    *this = ms[0];
}

void Mol2::write(std::string fn) {
    std::ofstream ofile(fn.c_str());
    write(ofile);
    ofile.close();
}

void Mol2::write(std::ostream &output) {
    write_molecule(output);
    write_atoms(output);
    write_bonds(output);
    write_substructures(output);
}

void Mol2::write_molecule(std::ostream &out) {
    out << "@<TRIPOS>MOLECULE" << std::endl;
    out << mol_name << std::endl;
    out << num_atoms;
    if (num_bonds != INT_MAX) {
        out << ' ' << num_bonds;
        if (num_subst != INT_MAX) {
            out << ' ' << num_subst;
            if (num_feat != INT_MAX) {
                out << ' ' << num_feat;
                if (num_sets != INT_MAX) {
                    out << ' ' << num_sets;
                }
            }
        }
    }
    out << std::endl;

    out << mol_type << std::endl;
    out << charge_type << std::endl;

    if (!status_bits.empty()) out << status_bits << std::endl;
    if (!mol_comment.empty()) out << mol_comment << std::endl;
}

void Mol2::write_atoms(std::ostream &out) {
    out << "@<TRIPOS>ATOM" << std::endl;
    int i = 0;
    for (auto &&atom : atoms) {
        out << i+1 << ' ' << atom.name << ' ' << atom.x << ' ' << atom.y << ' ' << atom.z << ' ' << atom.type;
        if (atom.subst_id != INT_MAX) {
            out << ' ' << atom.subst_id+1;
            if (!atom.subst_name.empty()) {
                out << ' ' << atom.subst_name;
                if (atom.charge != DBL_MAX) {
                    out << ' ' << atom.charge;
                    if (!atom.status_bit.empty()) {
                        out << ' ' << atom.status_bit;
                    }
                }
            }
        }
        out << std::endl;
        i++;
    }
}

void Mol2::write_bonds(std::ostream &out) {
    if (bonds.empty()) return;

    out << "@<TRIPOS>BOND" << std::endl;
    int i = 0;
    for (auto &&bond : bonds) {
        out << i+1 << ' ' << bond.origin_atom_id+1 << ' ' << bond.target_atom_id+1 << ' ' << bond.type;
        if (!bond.status_bits.empty()) out << ' ' << bond.status_bits;
        out << std::endl;
        i++;
    }
}

void Mol2::write_substructures(std::ostream &out) {
    if (substructures.empty()) return;

    out << "@<TRIPOS>SUBSTRUCTURE" << std::endl;
    int i = 0;
    for (auto &&subst : substructures) {
        out << i+1 << ' ' << subst.name << ' ' << subst.root_atom;
        if (!subst.type.empty()) {
            out << ' ' << subst.type;
            if (subst.dict_type != INT_MAX) {
                out << ' ' << subst.dict_type;
                if (!subst.chain.empty()) {
                    out << ' ' << subst.chain;
                    if (!subst.sub_type.empty()) {
                        out << ' ' << subst.sub_type;
                        if (subst.inter_bonds != INT_MAX) {
                            out << ' ' << subst.inter_bonds;
                            if (!subst.status.empty()) {
                                out << ' ' << subst.status;
                                if (!subst.comment.empty()) {
                                    out << ' ' << subst.comment;
                                }
                            }
                        }
                    }
                }
            }
        }
        out << std::endl;
        i++;
    }
}

} // namespace mol2

} // namespace jnc


#include "pdb_writer.hpp"
#include "pdb_chain.hpp"

namespace jnc {

namespace pdb {

V<const Atom *> Chain::patoms() const {
    V<const Atom *> as;
    for (auto && res : *this) {
        for (auto && atom : res) {
            as.push_back(&atom);
        }
    }
    return std::move(as);
}

V<Atom *> Chain::patoms() {
    V<Atom *> as;
    for (auto && res : *this) {
        for (auto && atom : res) {
            as.push_back(&atom);
        }
    }
    return std::move(as);
}

void Chain::read(const std::string & fn) {
    Pdb mol(fn);
    for (auto && chain : mol[0]) {
        for (auto && res : chain) {
            push_back(std::move(res));
        }
    }
}

std::ostream &operator <<(std::ostream &output, const Chain &chain) {
    PdbWriter l(output);
    l.write_model_begin();
    l.write_chain(chain);
    l.write_model_end();
    l.write_file_end();
    return output;
}

} // namespace pdb

} // namespace jnc


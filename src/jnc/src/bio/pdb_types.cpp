#include "pdb_types.hpp"

#include <map>
#include <string>

namespace jnc {

namespace pdb {

using namespace std;

int get_atom_element(const string &atom_name) {
#define ELT(mol_type, res_name, res_type, atom_name, atom_type, atom_element) {atom_name, atom_element}
#define SEP ,
    static map<string, int> name_map { JN_PDB_ATOM_TABLE };
#undef ELT
#undef SEP
    try {
        return name_map.at(atom_name);
    }
    catch (...) {
        return ::jnc::get_atom_element(lexical_cast<string>(atom_name[0]));
    }
}

}  // namespace pdb

}

